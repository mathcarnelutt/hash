#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "hash.h"

struct Ponto {
  float x, y;
};
typedef struct Ponto *Ponto;

struct Par {
  char *chave;
  Ponto valor;
};

typedef struct Par Par;

Ponto newPonto(float x, float y) {
  Ponto this = calloc(1, sizeof(struct Ponto));
  this->x    = x;
  this->y    = y;
  return this;
}

char *pontoString(void *_ponto) {
  Ponto ponto = _ponto;
  
  if (!ponto)
    return calloc(1, 1);
  
  char *saida = malloc(8);
  sprintf(saida, "(%2.0f,%2.0f)", ponto->x, ponto->y);
  return saida;
}

int main() {
  HashTable tabela = HashTable_t.create(4);

  Par infos[] = {    
    {.chave = "Matheus", .valor = newPonto(17,  3)},
    {.chave = "Antonio", .valor = newPonto(30, 11)},
    {.chave = "Mari",    .valor = newPonto(17,  4)},
    {.chave = "Taisa",   .valor = newPonto(30, 11)},
    {.chave = "Daniel",  .valor = newPonto(25,  7)},
    {.chave = "Luscas",  .valor = newPonto(25,  7)},
    {.chave = "Murilo",  .valor = newPonto(25,  7)},
    {.chave = "Rajjer",  .valor = newPonto(25,  7)},
  };

  int tamanho = sizeof(infos) / sizeof(*infos);

  for (int i = 0; i < tamanho; i++) {
    HashTable_t.insert(tabela, infos[i].chave, infos[i].valor);
    HashTable_t.print(tabela, pontoString, stdout);
    printf("=======================\n");
  }

  if (HashTable_t.exists(tabela, "Mari"))
    printf("Existe\n");
  else
    printf("Nao existe\n");
  
  HashTable_t.destroy(tabela, free, false);

  return 0;
}